# Repository of example of coupled hydrogeophysical implementation

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hkex%2Fcoupled-projects/main)

Contains scripts or example of coupled project implementations.

- `column-hydrus-timelapse`: 1D column with simple flow transport (Hydrus) during infiltration and drainage and timelapse ERT monitoring.
- `phydrus_geophys`: 1D column with simple flow transport (Hydrus) during infiltration and drainage and timelapse ERT monitoring using `phydrus`, coupled with ERT and EMI using `resipy` and `emagpy` respectively.
