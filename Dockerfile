FROM jupyter/scipy-notebook:3395de4db93a

ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}

USER root

RUN apt-get update
RUN apt-get -y install wine-stable
RUN dpkg --add-architecture i386
RUN apt-get update
RUN apt-get -y install wine32
RUN pip install notebook numpy matplotlib pandas scipy requests psutil seaborn

USER ${NB_USER}
COPY . ${HOME}
USER root
RUN chown -R ${NB_UID} ${HOME}
USER ${NB_USER}
