#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  7 15:00:16 2021
SUTRA HANDLER 
@author: jimmy
"""
import os, platform, warnings, shutil  
from subprocess import PIPE, Popen
import datetime as dt
import numpy as np 
import matplotlib.pyplot as plt
import pandas as pd 
from tqdm import tqdm
from joblib import Parallel, delayed

#%% matric potential functions 
def normparam(val,sat_val,res_val) -> float: 
    """ normalised parameter
    """
    val = np.array(val)
    return (val-res_val)/(sat_val-res_val)

def vgCurve(suction,ECres,ECsat,alpha,n,m=None):
    """van Genuchtan curve. 
    
    Parameters
    ------------
    suction: array, float
        Suction values in kPa. 
    vol_res: float
        residual value
    vol_sat: float
        saturated value
    alpha: float
        air entry pressure, can be fitted with a curve or found experimentally
    n: float
        van Genuchtan 'n' parameter. Not the same as archie's exponents. 
    m: float, optional
        van Genuchtan 'n' parameter. If left as none then m = 1 -1/n
    
    Returns
    -----------
    eff_vol: array, float
        normalised value 
    """
    if m is None:
        m = 1 - (1/n) # defualt definition of m
    
    dVol = ECsat - ECres # maximum change in water content
    denom = 1 + (alpha*suction)**n
    eff_vol = ECres + dVol/(denom**m)
    
    return eff_vol

def vgCurvefit(suction,res,sat,alpha,n,m=None):
    """van Genuchtan curve used in curve fitting. 
    
    Parameters
    ------------
    suction: array, float
        Suction values in kPa. 
    res: float
        residual value
    sat: float
        saturated value
    alpha: float
        air entry pressure, can be fitted with a curve or found experimentally
    n: float
        van Genuchtan 'n' parameter. Not the same as archie's exponents. 
    m: float, optional
        van Genuchtan 'n' parameter. If left as none then m = 1 -1/n
    
    Returns
    -----------
    val: array, float
        EC or water content value (not normalised)
    """
    if m is None:
        m = 1 - (1/n) # defualt definition of m
    
    dVol = sat - res # maximum change in water content value 
    denom = 1 + (alpha*suction)**n
    eff = res + dVol/(denom**m) # effective water content value 
    
    return (eff*dVol )+ res

def normVGcurve(suction,alpha,n,m=None):
    """Van Genuchtan curve but returns normalised parameters. 
    
    Parameters
    ------------
    suction: array, float
        Suction values in kPa. 
    alpha: float
        Air entry pressure, can be fitted with a curve or found experimentally
    n: float
        Van Genuchtan 'n' parameter. Not the same as archie's exponents. 
    m: float, optional
        Van Genuchtan 'm' parameter. If left as none then m = 1 -1/n
    
    Returns
    -----------
    norm_param: array, float
        normalised water content 
    """
    if m is None:
        m = 1 - (1/n) # defualt definition of m
    
    denom = 1 + (alpha*suction)**n
    norm_param = denom**-m
    return norm_param

#inverse of the above 
def invVGcurve(water_content,sat,res,alpha,n,m = None):
    """
    Inverse of van genutchen curve where we solve for suction not moisture content. 

    Parameters
    ----------
    water_content : float, nd array (of float)
        Water content as a fraction 
    sat : float 
        Saturated water content (normally 1).
    res : float 
        Residual water content.
    alpha : float 
        alpha parameters.
    n : float 
        Van genutchen n parameter
    m : float, optional
        M parameter. The default is None (computed as a function of n)

    Returns
    -------
    pressure: float, nd array 
        Matric potential (positive).

    """
    if m is None:
        m=1-(1/n)
        
    step1 = (sat-res)/(water_content-res)
    step2 = step1**(1/m) - 1
    step3 = step2**(1/n)
    step4 = step3/alpha 
    
    return step4 

def waxmanSmit(saturation,F,sigma_w,sigma_s): # convert moisture content to resistivity via waxman smits 
    """
    
    Parameters
    ----------
    saturation: float
        Saturation (in fraction)
    F: float
        Formation factor 
    sigma_w: float 
        Pore fluid conductivity 
    sigma_s: float 
        Grain surface conductivity 
    """    
    sigma = (1/F)*((sigma_w*saturation**2)+(sigma_s*saturation))
    
    return sigma 

def invWaxmanSmit(sigma,F,sigma_w,sigma_s):#working!
    """Convert true rock resistivity into a water saturation according waxman-smit
    model.
    
    Parameters
    ---------- 
    sigma: float 
        total rock conductivity 
    F: float
        Formation factor 
    sigma_w: float 
        Pore fluid conductivity 
    sigma_s: float 
        Grain surface conductivity 
    
    Returns 
    ---------- 
    Sw: float
        water (or conductive phase) saturation 
    """
    #minimization scheme start
    trial_Sw=0.5#trial Sw

    #first calculation
    calc_Sw=((sigma_w*(trial_Sw**2))-(sigma*F))/-sigma_s
    delta_Sw=calc_Sw-trial_Sw
    trial_Sw=trial_Sw+delta_Sw
    #minmisation while loop 
    count=0
    while abs(delta_Sw)>0.0001:
        calc_Sw=((sigma_w*(trial_Sw**2))-(sigma*F))/-sigma_s
        delta_Sw=calc_Sw-trial_Sw
        trial_Sw=trial_Sw+delta_Sw
        count+=1
        if count>30:
            print('warning')
            break
    return trial_Sw

#%% statistics / utility functions 

def rmse(d0,dm):
    N = len(d0)
    diff = d0 - dm
    sumup = np.sum(diff**2)
    return np.sqrt(sumup/N)

def chi2(meas_errors,residuals):
    n = len(residuals)
    xsum = 0 
    for i in range(n):
        x = (residuals[i]**2) / (meas_errors[i]**2) 
        xsum += x 
    return xsum/n   

def chi2_log(meas_errors,residuals):
    n = len(residuals)
    r = np.matrix(np.log10(np.abs(residuals))).T
    W = np.matrix(np.diag(1/np.log10(meas_errors**2)))
    X_2 = r.T*W.T*W*r
    X2 = float(X_2)*(1/n)
    return X2     

def lfunc(meas_errors,residuals):
    comp1 = -0.5*np.log(np.pi*2*np.abs(meas_errors))
    comp2 = -0.5*((residuals**2) / (meas_errors**2))
    return -np.sum(comp1 + comp2)

def llfunc(meas_errors, residuals): 
    n = len(residuals)
    c = -0.5*np.log(2*np.pi) # constant 
    lsum = 0 
    for i in range(n):
        std = abs(meas_errors[i]) 
        var = meas_errors[i]**2
        res2 = residuals[i]**2 
        # li = np.exp(-res2/(2*var)) / np.sqrt(np.pi*2*var)
        lli = c - np.log(std) - res2/(2*var)
        lsum += lli 
    
    return lsum 

def normLike(meas_errors,residuals):
    """
    Compute the normalised likelihood ratio for a model. 

    Parameters
    ----------
    meas_errors : array like 
        Data errors.
    residuals : array like 
        d0 - dm.

    Returns
    -------
    normalised_likelihood: float 
        sum of normalised likelihoods over the number of measurements. 

    """
    n = len(residuals)
    psum = 0 
    c = -0.5*np.log(2*np.pi) # constant 
    for i in range(n):
        std = abs(meas_errors[i]) # standard deviation of point 
        var = meas_errors[i]**2 # varaince of point 
        res2 = residuals[i]**2 # square of residual 
        if std == 0 and res2==0: # in the case of a perfect data point normalised LR is 1 
            psum += 1 # so add 1 to psum and move on 
            continue 
        
        lli = c - np.log(std) - res2/(2*var) # log likelihood 
        lmle = c - np.log(std) # log max likelihood estimate 
        llr = lli - lmle # log likelihood ratio 
        lr = np.exp(llr) # convert back into normal space 
        psum += lr # add to total probability 
        
    return psum/n # normalise to between 0 and 1 

def convertTimeUnits(x,unit='sec'):
    if unit =='sec':
        return x 
    elif unit =='min':
        return x/60 
    elif unit =='hour':
        return x/(60*60)
    elif unit =='day':
        return x/(24*60*60)
    else:
        return x 
    
#%% return monte-carlo values 
def giveValues(v0,v1,n,dist='uniform'):
    if dist == 'ordered':
        return np.linspace(v0,v1,n)
    elif dist == 'logordered':
        return 10**np.linspace(np.log10(v0),np.log10(v1),n)
    elif dist =='uniform':
        return np.random.uniform(v0,v1,n)
    elif dist == 'loguniform':
        return 10**np.random.uniform(np.log10(v0),np.log10(v1),n)
    elif dist == 'normal':
        loc = (v0+v1)/2 
        scale = (v1-v0)/2 
        return np.random.normal(loc,scale,n)
    else: # shouldnt happen 
        raise Exception('Distribution type is unknown!')

def proposeStep(param,size,dist='normal'):
    nparam = len(param)
    size = np.array(size)
    param = np.array(param)
    if dist == 'normal':
        step = size * np.random.randn(nparam)
        return param + step 
    elif dist =='lognormal':
        logP = np.log10(param) # log of paramter 
        logstep = logP + (np.random.randn(nparam)*size) # propose model in log space 
        return 10**logstep 
    else: # shouldnt happen 
        raise Exception('Distribution type is unknown!')
    
#%% read and write functions 
def readNod(fname): 
    # open file 
    fh = open(fname,'r')
    
    #header lines 
    for i in range(3):
        _ = fh.readline()
    
    # first find the number of nodes 
    line = fh.readline()
    info = line.split()
    numnp = None 
    for i in range(len(info)):
        if 'Nodes' in info[i]:
            numnp = int(info[i-1])
    if numnp is None:
        raise ValueError("Couldn't parse the number of nodes, aborting...")
    
    #find nth time step 
    n = 0 
    c = 100 # fail safe if cannot find the next timelapse array 
    data = {}
    breakout = False 
    while True:
        line = fh.readline()
        c = 0 
        while 'TIME STEP' not in line:
            line = fh.readline()
            c+=1 
            if c==100 and n>0:
                breakout = True 
                break
        if breakout:
            break 
            
        #time step info 
        info = line.replace('#','').split()
        try:
            _ = float(info[4])
            _ = float(info[7])
        except:
            info[4] = 'nan'
            info[7] = 'nan'
            
        stepinfo = {'TIME STEP':int(info[2]),'Duration':float(info[4]),
                    'Time':float(info[7])}
        
        # get names of colums 
        _ = fh.readline()
        names_line = fh.readline().replace('#','')
        columns = names_line.split()
        ncol = len(columns)
        
        #create a structure for storing information 
        step = {}
        for column in columns:
            step[column] = []
            
        for i in range(numnp):
            values = fh.readline().split() # values for each node at step 
            for i in range(ncol):
                column = columns[i]
                if values[i] == 'NaN': # catch nan
                    values[i] = 'nan'
                if column == 'Node':
                    step[column].append(int(values[i]))
                else:
                    step[column].append(float(values[i]))
        
        data['step%iinfo'%(n)]=stepinfo
        data['step%i'%(n)]=step
        n+=1 
        
        if fh.readline() == '':# or c==100:
            break 
        
    fh.close()
    return data,n 

#%% run sutra (in parallel)
def doSUTRArun(wd,execpath=None): # single run of sutra 
    if wd is None:
        raise Exception('Working directory needs to be set')
    if execpath is None:
        raise Exception('Executable path needs to be set')
    
    if platform.system() == "Windows":#command line input will vary slighty by system 
        cmd_line = [execpath]
    
    elif platform.system() == 'Linux':
        cmd_line = [execpath] # using linux version if avialable (can be more performant)
        if '.exe' in execpath: # assume its a windows executable 
            cmd_line.insert(0,'wine') # use wine to run instead 
    else:
        raise OSError('Unsupported operating system') # if this even possible? BSD maybe. 

    ERROR_FLAG = False # changes to true if sutra causes an error, and results will not be read 

    p = Popen(cmd_line, cwd=wd, stdout=PIPE, stderr=PIPE, shell=False)#run gmsh with ouput displayed in console
    while p.poll() is None:
        line = p.stdout.readline().rstrip()
        if line.decode('utf-8') != '':
            # dump(line.decode('utf-8'))
            if 'ERROR' in line.decode('utf-8'):
                ERROR_FLAG = True 
    
    if ERROR_FLAG:
        return {},0 # return an empty result 
    
    # now read in result as a dictionary 
    files = os.listdir(wd)
    fname = '.nod'
    for f in files:
        if f.endswith('.nod'):
            fname = os.path.join(wd,f) #  we have found the .nod file 
            break 
    data, n = readNod(fname) 
    
    return data,n 

#%% run R2/R3t (in parrallel probably)
# protocol parser for 2D/3D and DC/IP
def protocolParser(fname, ip=False, fwd=False):
    """
    <type>     <ncol>
    DC 2D         6
    DC 2D + err   7
    DC 2D + fwd   7
    IP 2D         7
    IP 2D + err   9
    IP 2D + fwd   8
    DC 3D         10
    DC 3D + err   11
    DC 3D + fwd   11
    IP 3D         11
    IP 3D + err   13
    IP 3D + fwd   12
    
    format:
    R2   :5,7,7,7,7,20,15
    cR2  :4,4,4,4,4,16,14,16
    R3t  :5,7,4,7,4,7,4,7,4,20,15
    cR3t :5,7,4,7,4,7,4,7,4,20,15,15
    """
    # method 1: np.genfromtxt and fallback to pd.read_fwf
    try:
        # this should work in most cases when there is no large numbers
        # that mask the space between columns
        x = np.genfromtxt(fname, skip_header=1) # we don't know if it's tab or white-space
    except Exception as e: # if no space between columns (because of big numbers or so, we fall back)
        # more robust but not flexible to other format, this case should only be met in fwd
        # we hope to be able to determine ncols from genfromtxt()
        a = np.genfromtxt(fname, skip_header=1, max_rows=2)
        threed = a.shape[1] >= 10
        if threed is False and ip is False: # 2D DC
            x = pd.read_fwf(fname, skiprows=1, header=None, widths=[5,7,7,7,7,20,15]).values
        elif threed is False and ip is True: # 2D IP
            x = pd.read_fwf(fname, skiprows=1, header=None, widths=[4,4,4,4,4,16,14,16]).values
        elif threed is True and ip is False: # 3D DC
            x = pd.read_fwf(fname, skiprows=1, header=None, widths=[5,7,4,7,4,7,4,7,4,20,15]).values
        elif threed is True and ip is True: # 3D IP
            x = pd.read_fwf(fname, skiprows=1, header=None, widths=[5,7,4,7,4,7,4,7,4,20,15,15]).values
        else:
            raise ValueError('protocolParser Error:', e)
    
    if len(x.shape) == 1: # a single quadrupole
        x = x[None,:]
    if fwd:
        x = x[:,:-1] # discard last column as it is appRes
    if ip:
        colnames3d = np.array(['index','sa','a','sb','b','sm', 'm','sn','n','resist','ip','magErr','phiErr'])
        colnames2d = np.array(['index','a','b','m','n','resist','ip','magErr','phiErr'])
    else:
        colnames3d = np.array(['index','sa','a','sb','b','sm', 'm','sn','n','resist','magErr'])
        colnames2d = np.array(['index','a','b','m','n','resist','magErr'])
    ncol = x.shape[1]
    if ncol <= len(colnames2d): # it's a 2D survey
        colnames = colnames2d[:ncol]
    else: # it's a 3D survey
        colnames = colnames3d[:ncol]
        
    df = pd.DataFrame(x, columns=colnames)
    df = df.astype({'a':int, 'b':int, 'm':int, 'n':int})
    if 'sa' in df.columns:
        df = df.astype({'sa':int, 'sb':int, 'sm':int, 'sn':int})
        elec = np.vstack([df[['sa','a']].values, df[['sb','b']].values,
                          df[['sm','m']].values, df[['sn','n']].values])
        uelec = np.unique(elec, axis=0)
        dfelec = pd.DataFrame(uelec, columns=['string', 'elec'])
        dfelec = dfelec.sort_values(by=['string','elec']).reset_index(drop=True)
        dfelec['label'] = dfelec['string'].astype(str) + ' ' + dfelec['elec'].astype(str)
        dfelec = dfelec.drop(['string', 'elec'], axis=1)
        df['a2'] = df['sa'].astype(str) + ' ' + df['a'].astype(str)
        df['b2'] = df['sb'].astype(str) + ' ' + df['b'].astype(str)
        df['m2'] = df['sm'].astype(str) + ' ' + df['m'].astype(str)
        df['n2'] = df['sn'].astype(str) + ' ' + df['n'].astype(str)
        df = df.drop(['a','b','m','n','sa','sb','sm','sn'], axis=1)
        df = df.rename(columns={'a2':'a','b2':'b','m2':'m','n2':'n'})
    else:
        uelec = np.unique(df[['a','b','m','n']].values.flatten()).astype(int)
        dfelec = pd.DataFrame(uelec, columns=['label'])
        dfelec = dfelec.astype({'label': str})
    dfelec['x'] = np.arange(dfelec.shape[0])
    dfelec['y'] = 0
    dfelec['z'] = 0
    dfelec['buried'] = False
    dfelec['remote'] = False
    df = df.astype({'a':str, 'b':str, 'm':str, 'n':str})
    if 'ip' not in df.columns:
        df['ip'] = np.nan
    return dfelec, df

def runR2runs(wd,execpath=None,surrogate='resistivity.dat'):
    """
    Run R2 (or R3t) for multiple forward runs

    Parameters
    ----------
    wd : str
        Path to R2 working directory.
    execpath : str
        Path to executable file, needs to be defined. The default is None.
    surrogate: str, optional 
        Name of surrogate resistivity file which detials the starting resistivities 
    
    Raises
    ------
    Exception
        DESCRIPTION.
    OSError
        DESCRIPTION.

    Returns
    -------
    TYPE
        DESCRIPTION.
    TYPE
        DESCRIPTION.

    """
    if wd is None:
        raise Exception('Working directory needs to be set')
    if execpath is None:
        raise Exception('Executable path needs to be set')
    
    if platform.system() == "Windows":#command line input will vary slighty by system 
        cmd_line = [execpath]
    
    elif platform.system() == 'Linux':
        cmd_line = [execpath] # using linux version if avialable (can be more performant)
        if '.exe' in execpath: # assume its a windows executable 
            cmd_line.insert(0,'wine') # use wine to run instead 
    else:
        raise OSError('Unsupported operating system') # if this even possible? BSD maybe. 
    
    ERROR_FLAG = False 
    
    entries = os.listdir(wd)
    steps = []
    n = 0 
    for e in entries:
        # determine if resistivity vector 
        if 'step' in e:
            steps.append(e)
            n+=1 
            
    data = {}
    for i,step in enumerate(sorted(steps)):
        shutil.copy(os.path.join(wd,step),
                    os.path.join(wd,surrogate))
        p = Popen(cmd_line, cwd=wd, stdout=PIPE, stderr=PIPE, shell=False)#run R2 with ouput displayed in console
        while p.poll() is None and not ERROR_FLAG:
            line = p.stdout.readline().rstrip()
            if line.decode('utf-8') != '':
                # dump(line.decode('utf-8'))
                if 'ERROR' in line.decode('utf-8'):
                    ERROR_FLAG = True 
        if ERROR_FLAG:
            return {},0 # return an empty result if error thrown, otherwise ... 
        
        # wait till finish and parse result
        fwd_file = 'R2_forward.dat'
        if 'R3t.exe' in execpath:
            fwd_file = 'R3t_forward.dat'
            
        elec,df = protocolParser(os.path.join(wd,fwd_file))
        data[i] = df['resist'].values 
    
    return data,n 

def runR2runsP(wd,execpath=None,surrogate='resistivity.dat',ncpu=4):
    """
    Run R2 (or R3t) for multiple forward runs (parrallel version)

    Parameters
    ----------
    wd : str
        Path to R2 working directory.
    execpath : str
        Path to executable file, needs to be defined. The default is None.
    surrogate: str, optional 
        Name of surrogate resistivity file which detials the starting resistivities 
    
    Raises
    ------
    Exception
        DESCRIPTION.
    OSError
        DESCRIPTION.

    Returns
    -------
    TYPE
        DESCRIPTION.
    TYPE
        DESCRIPTION.

    """
    if wd is None:
        raise Exception('Working directory needs to be set')
    if execpath is None:
        raise Exception('Executable path needs to be set')
    
    if platform.system() == "Windows":#command line input will vary slighty by system 
        cmd_line = [execpath]
    
    elif platform.system() == 'Linux':
        cmd_line = [execpath] # using linux version if avialable (can be more performant)
        if '.exe' in execpath: # assume its a windows executable 
            cmd_line.insert(0,'wine') # use wine to run instead 
    else:
        raise OSError('Unsupported operating system') # if this even possible? BSD maybe. 
    
    
    
    entries = os.listdir(wd)
    steps = []
    n = 0 
    for e in entries:
        # determine if resistivity vector 
        if 'step' in e:
            steps.append(e)
            n+=1 
            
    data = {}
    def loop(i,step):
        ERROR_FLAG = False 
        shutil.copy(os.path.join(wd,step),
                    os.path.join(wd,surrogate))
        p = Popen(cmd_line, cwd=wd, stdout=PIPE, stderr=PIPE, shell=False)#run R2 with ouput displayed in console
        while p.poll() is None and not ERROR_FLAG:
            line = p.stdout.readline().rstrip()
            if line.decode('utf-8') != '':
                # dump(line.decode('utf-8'))
                if 'ERROR' in line.decode('utf-8'):
                    ERROR_FLAG = True 
        if ERROR_FLAG:
            return [] # return an empty result if error thrown, otherwise ... ]]
        
        # wait till finish and parse result
        fwd_file = 'R2_forward.dat'
        if 'R3t.exe' in execpath:
            fwd_file = 'R3t_forward.dat'
            
        elec,df = protocolParser(os.path.join(wd,fwd_file))
        return df['resist'].values 

    desc = 'Running R2 runs'
    pout = Parallel(n_jobs=ncpu)(delayed(loop)(i,steps[i]) for i in tqdm(range(n),ncols=100,desc=desc,disable=True))

    for i in range(n):
        data[i] = pout[i] 
        
    return data,n 

#%% read in run parameters 
def readParam(fname):
    fh = open(fname,'r')
    lines = fh.readlines()
    param = {}
    for i in range(1,len(lines)):
        line = lines[i].split()
        key = line.pop(0)
        param[key] = [float(x) for x in line]
    fh.close()
    return param 



    
#%% master class for handling sutra 
class handler: 
    param = {'res':[0.3],
             'ecres':None,
             'sat':[1.0],
             'ecsat':None, 
             'alpha':[5e-5],
             'vn':[2]} # van genutchen parameter arrays (populated with default values)
    
    waxman = {'F':None,
              'sigma_w':None,
              'sigma_s':None}
    
    parg0 = {} # lower bound for parameter ranges in monte carlo searches 
    
    parg1 = {} # upper bound for parameter ranges in monte carlo searches 
    
    pargs = {} # starting place for mcmc approach 
    
    psize = {} # step sizes for mc approach 
    
    pdist = {'k':'uniform',
             'theta':'uniform', 
             'res':'uniform',
             'sat':'uniform',
             'alpha':'uniform',
             'vn':'normal'} # types of distributions (pdfs) used for the monte carlo modelling 

    setupinp = None # store for initial model setup (populated by setupInp) 
    
    pdirs = [] # directorys for running parallel runs 
    nruns = 0 
    
    template = 'r{:0>5d}' # template for parallel runs 
    
    def __init__(self,dname='SUTRAwd',name='sutra', title = None, 
                 subtitle = None, tlength=3600, ifac=5, ncpu=6, iobs=None,
                 saturated = False, flow = 'transient', transport='transient',
                 sim_type='solute'):
        if not os.path.exists(dname):
            os.mkdir(dname)
        self.dname = dname 
        self.name = name 
        self.title = title 
        self.subtitle = subtitle 
        if self.title is None: # set run title 
            self.title = 'SUTRA run for %s'%name 
        if self.subtitle is None: # if no subtitle just assign the date 
            self.subtitle = str(dt.datetime.now())
        self.tlength = tlength # length of each time step (inseconds)
        self.ifac = ifac # factor which to break down, internal time cycling in sutra 
        self.maxIter = 500 
        if iobs == None: # set the factor at which to make observations 
            self.iobs = self.ifac 
        else:
            self.iobs = iobs 
        
        # model type 
        self.sim_type = sim_type
        self.saturated = saturated # True if only saturated flow, otherwise unsaturated 
        self.flow = flow # flow type, steady or transient
        self.transport = transport # transport type 
        
        # 'setable' attributes 
        self.mesh = None 
        self.nzones = None 
        # self.nodePor = None 
        # self.elemPerm = None 
        self.execpath = None 
        self.closeFigs = True 
        
        # attributes used in plotting 
        self.attribute = 'Saturation'
        self.vmin = -0.01
        self.vmax = 1.01 
        self.xlim = None 
        self.ylim = None 
        self.vlim = None 
        
        # results variables 
        self.nodResult = {} 
        self.nodResultMulti = {} 
        self.resultNsteps = 0
        self.resFwdMdls = {} # holds forward model results from R family of codes 
        
        # set the number of cpus to use in parallised functions 
        self.ncpu = ncpu 
            
    # THESE SET FUNCTIONS MUST BE RUN FIRST 
    def setMesh(self,mesh):
        """
        Set mesh object for sutra handler 

        Parameters
        ----------
        mesh : class 
            Mesh class of ResIPy.meshTools.

        """
        self.mesh = mesh 
        self.mesh.df = pd.DataFrame()
        self.mesh.ptdf = pd.DataFrame()
        self.mesh.cellCentres()
        
    def getSurfaceArea(self):
        """
        Gets the surface area multiplier to apply to source / sink nodes 

        Returns
        -------
        None.

        """
        if self.mesh is None: 
            raise Exception ('mesh class has not been set!')
        # assume 2d for now 
        X,Z = self.mesh.extractSurface() 
        dx = np.zeros(len(X))
        dx[0] = np.abs(X[1] - X[0])
        for i in range(1,len(X)):
            dx[i] = np.abs(X[i] - X[i-1])
        
        return dx 
        
        
    def setZone(self,val=1):
        if self.mesh is None: 
            raise Exception ('mesh class has not been set!')
        if isinstance(val,float) or isinstance(val,int):
            self.mesh.ptdf['zone'] = [int(val)]*self.mesh.numnp 
        else:
            if self.mesh.numnp == len(val):    
                self.mesh.ptdf['zone'] = val 
            else:
                raise ValueError('mis match in node array lengths')
        self.nzones = np.unique(self.mesh.ptdf['zone'].values).shape[0] # get the number of zones 
        # set the zones for elements as well 
        self.mesh.node2ElemAttr(self.mesh.ptdf['zone'].values,'zone')
        #force values to be int 
        self.mesh.df['zone'] = np.asarray(np.round(self.mesh.df['zone']),dtype=int)
        
    
    def setPor(self,val=0.3):
        if self.mesh is None or self.nzones is None: 
            raise Exception ('mesh and zones need to be set first')
        if isinstance(val,float): # if single value then set to array like 
            self.mesh.ptdf.loc[:,'porosity'] = [val]*self.mesh.numnp 
        elif len(val) == self.nzones: # then assign by zone 
            self.mesh.ptdf.loc[:,'porosity'] = np.zeros(self.mesh.numnp)
            c = 0 
            for i in np.unique(self.mesh.ptdf['zone'].values):
                idx = self.mesh.ptdf['zone'].values == i
                self.mesh.ptdf.loc[idx,'porosity'] = val[c]
                c+=1 
        else: # assume that its the nodewise assigment 
            if len(val) != self.mesh.numnp:
                raise ValueError('mis match in node array lengths')
            self.mesh.ptdf.loc[:,'porosity'] = val 
            
    def setPerm(self,val=1e-10):
        if self.mesh is None or self.nzones is None: 
            raise Exception ('mesh and zones need to be set first')
        if isinstance(val,float): # if single value then set to array like 
            self.mesh.df.loc[:,'perm'] = [val]*self.mesh.numel 
        elif len(val) == self.nzones: # then assign by zone 
            self.mesh.df.loc[:,'perm'] = np.zeros(self.mesh.numel)
            c = 0 
            for i in np.unique(self.mesh.df['zone'].values):
                idx = self.mesh.df['zone'].values == i
                self.mesh.df.loc[idx,'perm'] = val[c]
                c+=1 
        else: # assume that its the nodewise assigment 
            if len(val) != self.mesh.numel:
                raise ValueError('mis match in element array lengths')
            self.mesh.df.loc[:,'perm'] = val 
            
    def setEXEC(self,execpath):
        if not isinstance(execpath,str):
            raise ValueError('input must be a string')
        if not os.path.exists(execpath):
            raise EnvironmentError('Executable file does not exist!')
        self.execpath = os.path.abspath(execpath) 

    def setupInp(self,times=[], source_node=[], source_val =None, 
                 pressure_node = [], pressure_val = None,
                 temp_node = [], temp_val = None, 
                 general_node = [], 
                 solver = 'DIRECT'): 
        """
        Setup input
        """
        self.setupinp = {'times':times,
                         'source_node':source_node,
                         'source_val':source_val,
                         'pressure_node':pressure_node,
                         'pressure_val':pressure_val,
                         'temp_node':temp_node,
                         'temp_val':temp_val, 
                         'general_node':general_node,
                         'solver':solver}
        
    # WRITE functions
    def writeInp(self):
        if self.setupinp is None:
            raise Exception('Initial setup has not been done yet!')
            
        times = self.setupinp['times']
        source_node = self.setupinp['source_node']
        source_val = self.setupinp['source_val']
        pressure_node = self.setupinp['pressure_node']
        pressure_val = self.setupinp['pressure_val']
        temp_node = self.setupinp['temp_node']
        temp_val = self.setupinp['temp_val']
        general_node = self.setupinp['general_node']
        solver= self.setupinp['solver']
        
        # mesh stats 
        if self.mesh is None: 
            raise Exception ('mesh class has not been set!')
        else: 
            numnp = self.mesh.node.shape[0]
            numel = self.mesh.connection.shape[0]
    
        fh = open(os.path.join(self.dname, self.name+'.inp'), 'w')
        # Title 1 see master document (sutra 2.2) pg 239 onwards
        fh.write('%s\n'%self.title)
        fh.write('%s\n'%self.subtitle)  # TITLE 2
    
        # Data Set 2A
        fh.write('# Data Set 2A\n')
        # Four words: sutra, version, and flow type
        fh.write("'SUTRA VERSION 3.0 %s TRANSPORT'\n"%self.sim_type.upper())
    
        # Data Set 2B
        fh.write('# Data Set 2B\n')
        fh.write("'2D IRREGULAR MESH'\n")  # mesh descriptor, NN1 NN2 (NN3)
    
        # Start_inp3
        # Data Set 3 - simulation control - see pg 243 of master doc and pg 51 of sutra 3.0 update doc
        fh.write('# Data Set 3\n')
        NUBC = 0  # len(source_node)#0
        NPBC = len(pressure_node)
        NSOP = len(source_node)
        NPBG =  len(general_node) # general nodes used as seepage for now 
    
        # numnp(NN), numel(NE), NPBC, NUBC, NSOP, NSOU, NPBG NUBG NOBS (line changed as of sutra 3.0?)
        fh.write('%i %i %i %i %i 0 %i 0 0\n' % (numnp, numel, NPBC, NUBC, NSOP, NPBG))
    
        # Start_inp4
        # Data Set 4
        fh.write('# Data Set 4\n')
        # CUNSAT CSSFLOW CSSTRA CREAD ISTORE
        sat_text = 'UNSATURATED'
        if self.saturated:
            sat_text = 'SATURATED'
        fh.write("'%s' '%s FLOW' '%s TRANSPORT' 'COLD'  9999\n"%(sat_text,
                                                                 self.flow.upper(),
                                                                 self.transport.upper()))
    
        # Data Set 5
        fh.write('# Data Set 5\n')
        # UP (reccomend = 0) | GNUP GNUU also shown in docs but not needed as of sutra 3.0
        fh.write('0.\n')
    
        # Data Set 6
        tmax = times[-1]
        nsteps =len(times)
        scalt = self.tlength #scale in seconds 
        ifac = self.ifac 
        tstep = np.mean(np.diff(times))
        istep = tstep/ifac  # step between internal time steps
        isteps = np.arange(0, tmax+istep, istep) 
    
        fh.write('# Data Set 6\n')
        fh.write('2 1 1\n')  # NSCH NPCYC NUCYC
        # old way to internal scheduling, we need a TIME_STEPS though
        fh.write("'TIME_STEPS' 'TIME CYCLE' 'ELAPSED' %i %i 0 1.e+99 1. 1 1. 1.e-20 1\n"%(scalt/ifac,len(isteps))) #SCHNAM SCHTYP CREFT SCALT NTMAX TIMEI TIMEL TIMEC NTCYC TCMULT TCMIN TCMAX
    
        # define time steps in input file here
        if self.flow.upper() == 'TRANSIENT':
            fh.write("'Rainfall' 'TIME LIST' 'ELAPSED' %3.2f %i\n" % (scalt, nsteps))
            c = 0
            for i in range(nsteps):
                fh.write("%3.2f " % (times[i]))
                if c == 10:
                    fh.write('\n')
                    c = 0
                c += 1
            fh.write('\n')
    
        fh.write('-\n')  # signals the last schedule
    
        # Data Set 7A
        fh.write('# Data Set 7A\n')
        fh.write('%i 2e3 2e3\n'%self.maxIter)  # ITRMAX RPMAX RUMAX
    
        if solver == 'DIRECT':
            # Data Set 7B - equation solver for pressure solution (i guess direct is okay for small problems)
            fh.write('# Data Set 7B\n')
            fh.write("'DIRECT'\n")  # CSOLVP | ITRMXP TOLP
        
            # Data Set 7C - equation solver for transport solution
            fh.write('# Data Set 7C\n')
            fh.write("'DIRECT'\n")  # CSOLVU | ITRMXU TOLU
        else: # use non linear solver 
            # Data Set 7B - equation solver for pressure solution (using non direct methods)
            fh.write('# Data Set 7B\n')
            fh.write("'GMRES' 1000 1e2\n") #CSOLVP | ITRMXP TOLP
        
            # Data Set 7C - equation solver for transport solution 
            fh.write('# Data Set 7C\n')
            fh.write("'GMRES' 1000 1e2\n") #CSOLVP | ITRMXP TOLP
    
        # Start_inpe  - output options
        fh.write('# Data Set 8\n')
        # NPRINT CNODAL CELMNT CINCID CPANDS CVEL CCORT CBUDG CSCRN CPAUSE
        fh.write("%i 'N' 'N' 'N' 'Y' 'N' 'N' 'Y' 'Y' 'Y' 'Data Set 8A'\n"%ifac)
        # NCOLPR NCOL ..
        fh.write("%i 'N' 'X' 'Y' 'U' 'S' 'P' '-' 'Data Set 8B'\n"%self.iobs)
        fh.write("%i 'E' 'VX' 'VY' '-' 'Data Set 8C'\n"%self.iobs)  # LCOLPR LCOL ..
    
        # Start_inp8D
        # OMIT when there are no observation points
    
        # Start_inp9 - output controls for boundary conditions
        # NBCFPR NBCSPR NBCPPR NBCUPR CINACT
        for i in range(6):
            fh.write("%i "%self.iobs) # output on time steps where data is 
        fh.write("Y Y Y 'Data Set 8E'\n") #NBCFPR NBCSPR NBCPPR NBCUPR CINACT
    
        # fluid properties
        fh.write('# Data Set 9\n')
        # COMPFL CW SIGMAW RHOWØ URHOWØ DRWDU VISCØ
        fh.write("4.47e-10 1. 1.e-09 1000. 0. 700. 0.001 'Data Set 9'\n") 
        
        # solid matrix properties
        fh.write('# Data Set 10\n')
        # COMPMA CS SIGMAS RHOS
        # fh.write("1.e-08 840. 1632. 2740. 'Data Set 10'\n")
        fh.write("1.e-08 0. 0. 2740. 'Data Set 10'\n") #COMPMA CS SIGMAS RHOS
    
        # adsorption parameters
        fh.write("'NONE' 'Data Set 11'\n")  # ADSMOD
    
        # Start_inp12
        # production of energy or solute mass
        fh.write('# Data Set 12\n')
        fh.write("0. 0. 0. 0. 'Data Set 12'\n")  # PRODFØ PRODSØ PRODF1 PRODS1
    
        # oreintation of gravity vector
        fh.write('# Data Set 13\n')
        fh.write("0. -9.81 0 'Data Set 13'\n")  # GRAVX GRAVY GRAVZ
    
        ### write out node matrix (dataset 14) ###            
        fh.write('# Data Set 14 (mesh nodes)\n')
        # NODE SCALX SCALY SCALZ PORFAC
        fh.write("'NODE' 1. 1. 1. 1. 'Data Set 14A'\n")
        zone = self.mesh.ptdf['zone']
        nodePor = self.mesh.ptdf['porosity']
        for i in range(numnp):
            # II NREG(II) X(II) Y(II) (Z(II)) POR(II)
            line = "%i %i %f %f 1. %f\n" % (
                i+1, zone[i], self.mesh.node[i, 0], self.mesh.node[i, 2], nodePor[i])
            fh.write(line)
    
        ### scale factors for element data ###
        fh.write('# Data Set 15 (element scalers)\n')
        # character PMAXFA PMINFA ANG1FA ALMAXF ALMINF ATMAXF ATMINF
        fh.write("'ELEMENT' 1 1 1 1 1 1 1\n"),
        elemPerm = self.mesh.df['perm']
        elemZone = self.mesh.df['zone']
        for i in range(numel):
            # L LREG(L) PMAX(L) PMIN(L) ANGLE1(L) ALMAX(L) ALMIN(L) ATMAX(L) ATMIN(L)
            #line = "%i %i %e %e 0. 0.5 0.5 0.5 0.5\n" %(i+1,elemZone[i],elemPerm[i],elemPerm[i])
            line = "%i %i %e %e 0. 1.0 1.0 1.0 1.0\n" %(i+1,elemZone[i],elemPerm[i],elemPerm[i])
            fh.write(line)
        
    
        ### write out source nodes ###
        if NSOP > 0:
            if source_val is None: 
                source_val = np.zeros(NSOP)
            elif isinstance(source_val,float) or isinstance(source_val,int):
                source_val = np.full(NSOP,source_val)
            for i in range(NSOP):
                line = "%i %.8e 0.0 'Data Set 17'\n" %(source_node[i],source_val[i])
                fh.write(line)
            fh.write("0 'Data Set 17'\n")
        
        ### write out pressure nodes ### 
        if NPBC>0: 
            if pressure_val is None:
                pressure_val = 0
            for i in range(NPBC):
                line = "%i %f 0. 'Data Set 19'\n"%(pressure_node[i],pressure_val)
                fh.write(line)
            fh.write("0 'Data Set 19'\n")
    
        ### write out temperatue nodes ###
        if NUBC > 0:
            for i in range(NUBC):
                line = "%i %f 'Data Set 20'\n" %(temp_node[i],temp_val[i])
                fh.write(line)
            fh.write("0 'Data Set 20'\n")
    
        ### add general flow / seepage nodes ###
        if NPBG > 0:
            fname21a = self.name+'.inp21A'
            fh.write("# Start new dataset 21A here\n")
            fh.write("@INSERT 95 '%s'\n"%fname21a)
            fh2 = open(os.path.join(self.dname, fname21a), 'w')
            for i in range(NPBG):
                #IPBG PBG1 QPBG1 PBG2 QPBG2 CPQL1 CPQL2 UPBGI CUPBGO UPBGO 
                line = "%i -1. 0. 0. 0. 'N' 'N' 0. 'REL' 0. 'Data Set 21A'\n"%general_node[i]
                fh2.write(line)
                # only doing general nodes for seepage for now 
            fh2.write("0 'Data Set 21A'\n")
            fh2.close()
        
        ### write out connection matrix ###
        fh.write('# Data Set 22 (connection matrix)\n')
        fh.write("'INCIDENCE'\n")
        for i in range(numel):
            line = "%i " % (i+1)  # LL IIN ...
            for j in range(4):
                line += "%i " % (self.mesh.connection[i, j] + 1)
            line += "\n"
            fh.write(line)
        fh.close()
    
    
    def writeBcs(self, times, source_node, rainfall, temps, surface_temp=None):
        """ 
        Datasets 3, 4, 5, and 6 of a “.bcs” file correspond to datasets 17, 18, 19, and 20, respectively,
        of the “.inp” file. (For example, “.bcs” dataset 5 and “.inp” dataset 19 both define
        specified-pressure nodes.) The formats of these four “.bcs” datasets parallel those of the
        corresponding “.inp” datasets.

        Parameters
        ----------
        times : TYPE
            DESCRIPTION.
        source_node : TYPE
            DESCRIPTION.
        rainfall : TYPE
            DESCRIPTION.
        temps : TYPE
            DESCRIPTION.
        surface_temp : TYPE, optional
            DESCRIPTION. The default is None.

        Returns
        -------
        None.

        """

        nsteps = len(times)
        nnodes = len(source_node)
        fh = open(os.path.join(self.dname, self.name+'.bcs'), 'w')
        ### time steps ###
        fh.write('# Dataset 1\n')
        fh.write("'Rainfall'\n")
    
        ### time dependent variables ###
        fh.write('# Dataset 3\n')
        NUBC = 0
        NSOP = len(source_node)
        if surface_temp is not None:
            NUBC = len(source_node)
    
        ### write out time steps (for sources) ###
        for i in range(nsteps):
            fh.write("'ts{:0>6d}' ".format(times[i]))
            if i == 0:
                # NSOP1, NSOU1, NPBC1, NUBC1, NPBG1, NUBG1 (according to sutra 3.0 docuementation)
                fh.write('%i 0 0 %i 0 0\n' % (NSOP, 0))
            else:
                fh.write('%i 0 0 %i 0 0\n' % (NSOP, NUBC))
            for j in range(nnodes):
                line = "%i %.8e %.8e\n" % (
                    source_node[j], rainfall[i, j], temps[i, j])
                fh.write(line)
            fh.write('0\n')
            # write out recorded surface temperatures
            if surface_temp is not None and i != 0:
                fh.write('# Dataset 6\n')
                for j in range(nnodes):
                    line = "%i %f\n" % (source_node[j], surface_temp[i, j])
                    fh.write(line)
                fh.write('0\n')
        fh.close()
    
    
    def writeIcs(self, pres, temps):
        # just needs the pressures and temperatures at each node
        fh = open(os.path.join(self.dname, self.name+'.ics'), 'w')
        fh.write('0 # starting time for init conditions\n')
        fh.write('# Dataset 2 - pressure conditions \n')
        # nb: this can be acquired with a single run of a steady state system
        fh.write("'NONUNIFORM'\n")
        for i in range(len(pres)):
            fh.write('%f\n' % pres[i])
    
        fh.write('# Dataset 3 - temperature conditions \n')
        # nb: this can be acquired with a single run of a steady state system
        fh.write("'NONUNIFORM'\n")
        for i in range(len(temps)):
            fh.write('%f\n' % temps[i])
        fh.close()
        
    def writeVg(self,swres=None,swsat=None,alpha=None,vn=None,dname=None):
        """
        Write unsaturated parameters to file 

        Parameters
        ----------
        swres : TYPE, optional
            DESCRIPTION. The default is None.
        swsat : TYPE, optional
            DESCRIPTION. The default is None.
        alpha : TYPE, optional
            DESCRIPTION. The default is None.
        vn : TYPE, optional
            DESCRIPTION. The default is None.
        dname : TYPE, optional
            DESCRIPTION. The default is None.

        """
        if swres is None: # override with default param 
            swres = self.param['res']
            swsat = self.param['sat']
            alpha = self.param['alpha']
            vn = self.param['vn']
            
        if dname is None: # fall back onto default directory 
            fpath = os.path.join(self.dname,'param.vg')
        else:
            fpath = os.path.join(dname,'param.vg')
        fh = open(fpath,'w')
        fh.write('%i\n'%self.nzones)
        for i in range(self.nzones):
            line = '{:d} {:f} {:f} {:f} {:f}\n'.format(i+1,swres[i],swsat[i],alpha[i],vn[i])
            fh.write(line)
        fh.close()
        
    def writeVg_OLD(self, swres, alpha, vn):
        fh = open(os.path.join(self.dname,'param.vg'),'w')
        nzones = len(swres)
        fh.write('%i\n'%nzones)
        for i in range(nzones):
            line = '{:d} {:f} {:f} {:f}\n'.format(i+1,swres[i],alpha[i],vn[i])
            fh.write(line)
        fh.close()

    # def writeGen(self,node):
    #     fh = open(os.path.join(self.dname, self.name+'.inp21A'), 'w')
    #     for i in range(len(node)):
    #         line = "%i -1.0 0. 0. 0. 'N' 'P' 0. 'REL' 0. 'Data Set 21A'" % node[i]
    #         fh.write(line+'\n')
    #     fh.write('0')
    #     fh.close()
    
    
    def writeFil(self, ignore=['BCOP', 'BCOPG']):
        # write out master file
        exts = ['INP', 'BCS', 'ICS', 'LST', 'RST',
                'NOD', 'ELE', 'OBS', 'BCOP', 'BCOPG', 'SMY'] # FILE EXTENSIONS 
        iunits = [50,    52,  55,     60,     66,
                  70,     75,     80,    92,       94,   98]# FILE READ UNITS (MAX SHOULD BE 98)
    
        fh = open(os.path.join(self.dname, 'SUTRA.FIL'), 'w')
        for i in range(len(exts)):
            e = exts[i]
            u = iunits[i]
            if e not in ignore:
                line = "{:_<7s} {:_<3d} '{:s}.{:s}'\n" .format(
                    e, u, self.name, e.lower())
                line = line.replace('_', ' ')
                # print(line.strip())
                fh.write(line)
        fh.close()
    
    # directory management 
    def clearDir(self):
        # clears directory of files made during runs 
        exts = ['INP', 'BCS', 'ICS', 'LST', 'RST',
                'NOD', 'ELE', 'OBS', 'BCOP', 'BCOPG', 'SMY'] # FILE EXTENSIONS 
        extsl = [e.lower() for e in exts] # lower case extensions 
        
        files = os.listdir(self.dname)
        for f in files:
            if f in exts or f in extsl: 
                os.remove(os.path.join(self.dname,f))
                
    def clearDirs(self):
        template = 'r{:0>5d}'
        for i in range(self.nruns): # loop through the number of runs 
            dpath = os.path.join(self.dname,template.format(i))
            if os.path.exists(dpath):
                shutil.rmtree(dpath) # remove directory 
                
    # run sutra 
    def runSUTRA(self,show_output=True,dump=print): # single run of sutra 
        if self.execpath is None:
            raise Exception('Executable path needs to be set first!')
        
        cwd = os.getcwd()
        if platform.system() == "Windows":#command line input will vary slighty by system 
            cmd_line = [self.execpath]
        
        elif platform.system() == 'Linux':
            cmd_line = [self.execpath] # using linux version if avialable (can be more performant)
            if '.exe' in self.execpath: # assume its a windows executable 
                cmd_line.insert(0,'wine') # use wine to run instead 
        else:
            raise Exception('Unsupported operating system') # if this even possible? BSD maybe. 
    
        ERROR_FLAG = False # changes to true if sutra causes an error 
        # change working directory 
        os.chdir(self.dname)
        # p = Popen(['cd',self.dname], stdout=PIPE, stderr=PIPE, shell=False)
        if show_output: 
            p = Popen(cmd_line, stdout=PIPE, stderr=PIPE, shell=False)#run gmsh with ouput displayed in console

            while p.poll() is None:
                line = p.stdout.readline().rstrip()
                if line.decode('utf-8') != '':
                    dump(line.decode('utf-8'))
                    if 'ERROR' in line.decode('utf-8'):
                        ERROR_FLAG = True 
        else:
            p = Popen(cmd_line, stdout=PIPE, stderr=PIPE, shell=False)
            p.communicate() # wait to finish
            
        os.chdir(cwd)
        
        if ERROR_FLAG:
            raise Exception('Looks like SUTRA run has failed, check inputs!')
            
    #get result functions 
    def getNod(self):
        #get nod file 
        files = os.listdir(self.dname)
        fname = '.nod'
        for f in files:
            if f.endswith('.nod'):
                fname = os.path.join(self.dname,f) # then we have found the .nod file 
                break 
        data, n = readNod(fname)
        self.nodResult = data 
        self.resultNsteps = n 
        print('%i time steps read'%n)  
        
    # get results 
    def getResults(self):
        self.getNod()
    
    # plotting functions 
    def plotNod(self, n=0): 
        """
        Plots an invidual time step of the nodewise simulation.

        Parameters
        ----------
        n : int, optional
            index of time step. The default is 0.
        attr : str, optional
            name of parameter to be plotted. The default is 'Saturation'.
        close: bool, optional 
            closes the figure once it has been plotted (result is saved inside 
            of working directory)

        """
        # plot sutra data 
        step=self.nodResult['step%i'%n]

        X = np.array(step['X']) # y values 
        Y = np.array(step['Y']) # x values 
        arr = np.array(step[self.attribute]) # array of node values 
        
        if any(np.isnan(arr)==True):
            return 
        
        fig, ax = plt.subplots()
        
        levels = np.linspace(self.vmin, self.vmax, 50)
        cax = ax.tricontourf(X,Y,arr,levels=levels)
        plt.colorbar(cax)
        timeprint = self.nodResult['step%iinfo'%n]['Time']
        ax.set_title('Step = %i, Time = %s (sec)'%(n,str(timeprint))) 
        if self.xlim is None:
            self.xlim = [np.min(X), np.max(X)]
        if self.ylim is None:
            self.ylim = [np.min(Y), np.max(Y)]
            
        ax.set_xlim(self.xlim)
        ax.set_ylim(self.ylim)
        
        dirname = os.path.join(self.dname,self.attribute)
        if not os.path.exists(dirname):
            os.mkdir(dirname)
            
        fig.savefig(os.path.join(dirname,'{:0>3d}.png'.format(n)))
        
        
        if self.closeFigs:
            plt.close(fig)
            
            
    # def setMeshAttribute(self, n=0):
    #     step=self.nodResult['step%i'%n]
    #     connec = self.mesh.connection
    #     node_arr = np.array(step[self.attribute]) # array of node values 
    #     arr = np.mean(node_arr[connec], axis = 1) # values of array on the node values 
    #     name = self.attribute + str(n)
    #     self.mesh.addAttribute(name,arr)
    #     return arr 
    
    def plotResults(self,parallel=False):
        # note that the parrallel option for this function doesnt work very well 
        n = self.resultNsteps
        desc = 'Plotting steps'
        warnings.filterwarnings("ignore")
        if not parallel: 
            for i in tqdm(range(n),ncols=100, desc=desc):
                self.plotNod(i)
        else:
            Parallel(n_jobs=self.ncpu)(delayed(self.plotNod)(i) for i in tqdm(range(n),ncols=100, desc=desc))#process over multiple cores 
        warnings.filterwarnings("default")
        
    def plotMesh(self,n=0, time_units='day',cmap='Spectral'):
        # covert to resistivity and map to elements
        step=self.nodResult['step%i'%n]
        node_arr = np.array(step[self.attribute])
        arr = self.mesh.node2ElemAttr(node_arr,self.attribute)

        fig, ax = plt.subplots()

        self.mesh.show(ax=ax, attr=self.attribute,
                       vmin=self.vmin,vmax=self.vmax,
                       color_map=cmap,zlim=self.ylim)#kwargs)
        timeprint = self.nodResult['step%iinfo'%n]['Time']
        timeprint = convertTimeUnits(timeprint,time_units)
        ax.set_title('Step = %i, Time = %s (%s)'%(n,str(timeprint),time_units)) 
        dirname = os.path.join(self.dname,self.attribute)
        if not os.path.exists(dirname):
            os.mkdir(dirname)
            
        fig.savefig(os.path.join(dirname,'{:0>3d}.png'.format(n)))
        
        if self.closeFigs:
            plt.close(fig)
    
    def plotMeshResults(self,time_units='day',cmap='Spectral'):
        n = self.resultNsteps
        desc = 'Plotting steps'
        warnings.filterwarnings("ignore")
        for i in tqdm(range(n),ncols=100, desc=desc):
            self.plotMesh(i,time_units,cmap)
            
    def plot1Dresult(self,n=0,x=None,time_units='day'):
        if x is None: 
            #get middle of array 
            x = np.median(self.mesh.node[:,0])
        step=self.nodResult['step%i'%n]
        X = np.array(step['X']) # y values 
        Y = np.array(step['Y']) # x values 
        arr = np.array(step[self.attribute]) # array of node values 
        
        if any(np.isnan(arr)==True):
            return 
        
        fig, ax = plt.subplots()
        
        idx = X == x # index of array 
        ax.plot(arr[idx], Y[idx],marker='x')
        timeprint = self.nodResult['step%iinfo'%n]['Time']
        timeprint = convertTimeUnits(timeprint,time_units)
        ax.set_title('Step = %i, Time = %s (%s)'%(n,str(timeprint),time_units)) 
        # if self.xlim is None:
        #     self.xlim = [np.min(X), np.max(X)]
        if self.vlim is None:
            self.vlim = [np.min(arr),np.max(arr)]
        if self.ylim is None:
            self.ylim = [np.min(Y), np.max(Y)]
            
        ax.set_xlim(self.vlim)
        ax.set_ylim(self.ylim)
        ax.set_xlabel(self.attribute)
        ax.set_ylabel('Depth (m)')
        
        dirname = os.path.join(self.dname,self.attribute)
        if not os.path.exists(dirname):
            os.mkdir(dirname)
            
        fig.savefig(os.path.join(dirname,'oneD{:0>3d}.png'.format(n)))
        
        if self.closeFigs:
            plt.close(fig)
            
    def plot1Dresults(self,clean_dir = True):
        n = self.resultNsteps
        desc = 'Plotting steps'
        warnings.filterwarnings("ignore")

        dirname = os.path.join(self.dname,self.attribute)
        if not os.path.exists(dirname):
            os.mkdir(dirname)
        elif clean_dir:
            files = os.listdir(dirname)
            for f in files: # remove old files 
                if f.endswith('.png'):
                    os.remove(os.path.join(dirname,f))
        
        for i in tqdm(range(n),ncols=100, desc=desc):
            self.plot1Dresult(i)
        

    def pres2res(self):
        if self.resultNsteps is None:
            raise Exception('Must run a model and get results first before converting to resistivity')
        if 'ecres' not in self.param.keys():
            raise Exception('ecres not in defined in paramters')
        if 'ecsat' not in self.param.keys():
            raise Exception('ecsat not in defined in paramters')
        
        cond = np.zeros(self.mesh.numnp) # array to hold conductivity values 
        res = np.zeros((self.mesh.numel,self.resultNsteps))
        ECsat = self.param['ecsat'] # parameter arrays 
        ECres = self.param['ecres']
        alpha = self.param['alpha']
        vn = self.param['vn']
        for i in range(self.resultNsteps):
        # for i in tqdm(range(self.resultNsteps),ncols=100): # generally this bit of code is very quick 
            pres = np.array(self.nodResult['step%i'%i]['Pressure']) # pressure in kpa 
            sat_idx = pres >= 0 # where saturation level is 1 
            unsat_idx = pres < 0 
            c=0
            for j in np.unique(self.mesh.ptdf['zone'].values):
                sat_idx = (self.mesh.ptdf['zone'].values == j) & sat_idx # index relevant to zone 
                unsat_idx = (self.mesh.ptdf['zone'].values == j) & unsat_idx 
                cond[sat_idx] = ECsat[c] # saturated region just the saturated conductivity  
                normcond = vgCurve(np.abs(pres[unsat_idx]), # need absolute values for function 
                                   ECres[c],ECsat[c],alpha[c],vn[c])
                # non - normalised conductivity 
                cond[unsat_idx] = (normcond*(ECsat[c]-ECres[c]))+ECres[c]
                c+=1 # add one to loop iterator index 
            
            # add conductivity values to data 
            self.nodResult['step%i'%i]['Conductivity'] = cond.tolist() 
            self.nodResult['step%i'%i]['Resistivity'] = (1/cond).tolist() 
            # covert to resistivity and map to elements 
            res[:,i] = self.mesh.node2ElemAttr(1/cond,'res%i'%i)
            
        return res 
    
    def sat2res(self):
        if self.resultNsteps is None:
            raise Exception('Must run a model and get results first before converting to resistivity')

        if self.waxman['F'] is None:
            raise Exception('Formation factors not defined')
        if self.waxman['sigma_w'] is None:
            raise Exception('Pore fluid conductivity not defined')
        if self.waxman['sigma_s'] is None:
            raise Exception('Grain surface conductivity not defined')
            
        cond = np.zeros(self.mesh.numnp) # array to hold conductivity values 
        res = np.zeros((self.mesh.numel,self.resultNsteps))
        
        multirun = False 
        if len(self.nodResultMulti.keys())>0:
            multirun = True 
            
        for i in range(self.resultNsteps):
        # for i in tqdm(range(self.resultNsteps),ncols=100): # generally this bit of code is very quick 
            sat = np.array(self.nodResult['step%i'%i]['Saturation']) # pressure in kpa 
            sat[sat>1] = 1 # cap saturation at 1 
            c=0
            cond = np.zeros_like(sat)
            for j in np.unique(self.mesh.ptdf['zone'].values):
                idx = self.mesh.ptdf['zone'].values == j 
                cond[idx] = waxmanSmit(sat[idx],
                                  self.waxman['F'][c],
                                  self.waxman['sigma_w'][c],
                                  self.waxman['sigma_s'][c])
                c+=1 # add one to loop iterator index 
            
            # add conductivity values to data 
            self.nodResult['step%i'%i]['Conductivity'] = cond.tolist() 
            self.nodResult['step%i'%i]['Resistivity'] = (1/cond).tolist() 
            # covert to resistivity and map to elements 
            res[:,i] = self.mesh.node2ElemAttr(1/cond,'res%i'%i)
            
            if not multirun:
                continue 
            
            # catch mutliple runs results as well 
            for key in self.nodResultMulti.keys():
                sat = np.array(self.nodResultMulti[key]['step%i'%i]['Saturation']) # pressure in kpa 
                sat[sat>1] = 1 # cap saturation at 1 
                c=0
                cond = np.zeros_like(sat)
                for j in np.unique(self.mesh.ptdf['zone'].values):
                    idx = self.mesh.ptdf['zone'].values == j 
                    cond[idx] = waxmanSmit(sat[idx],
                                      self.waxman['F'][c],
                                      self.waxman['sigma_w'][c],
                                      self.waxman['sigma_s'][c])
                    c+=1 # add one to loop iterator index 
                
                # add conductivity values to data 
                self.nodResultMulti[key]['step%i'%i]['Conductivity'] = cond.tolist() 
                self.nodResultMulti[key]['step%i'%i]['Resistivity'] = (1/cond).tolist() 
                
        return res 
    
    def sat2mois(self):
        if self.resultNsteps is None:
            raise Exception('Must run a model and get results first before converting to moisture contents')
            
        mois = np.zeros(self.mesh.numnp) # array to hold conductivity values 
        vmc = np.zeros((self.mesh.numel,self.resultNsteps))
            
        for i in range(self.resultNsteps):
        # for i in tqdm(range(self.resultNsteps),ncols=100): # generally this bit of code is very quick 
            sat = np.array(self.nodResult['step%i'%i]['Saturation']) # pressure in kpa 
            sat[sat>1] = 1 # cap saturation at 1 
            c=0
            for j in np.unique(self.mesh.ptdf['zone'].values):
                mois = self.param['theta_sat'][c]*sat
                c+=1 # add one to loop iterator index 
            
            # add moisture values to data 
            self.nodResult['step%i'%i]['Theta'] = mois.tolist() 

            # and map to elements 
            vmc[:,i] = self.mesh.node2ElemAttr(mois,'theta%i'%i)
            
        return vmc 
    
    def sat2res2phase(self,run_keys=None):
        if self.resultNsteps is None:
            raise Exception('Must run a model and get results first before converting to resistivity')

        if self.waxman['F'] is None:
            raise Exception('Formation factors not defined')
        if self.waxman['sigma_w0'] is None:
            raise Exception('Pore fluid conductivity 0 not defined')
        if self.waxman['sigma_w1'] is None:
            raise Exception('Pore fluid conductivity 1 not defined')
        if self.waxman['sat0'] is None:
            raise Exception('Need to define baseline saturation')
        if self.waxman['sigma_s'] is None:
            raise Exception('Grain surface conductivity not defined')
            
        if run_keys is None: 
            run_keys = self.nodResultMulti.keys() 
            
        cond = np.zeros(self.mesh.numnp) # array to hold conductivity values 
        res = np.zeros((self.mesh.numel,self.resultNsteps))
            
        for i in range(self.resultNsteps):
        # for i in tqdm(range(self.resultNsteps),ncols=100): # generally this bit of code is very quick 
            sat = np.array(self.nodResult['step%i'%i]['Saturation']) # pressure in kpa 
            sat[sat>1] = 1 # cap saturation at 1 
            c=0
            cond[:] = 0 
            for j in np.unique(self.mesh.ptdf['zone'].values):
                idx = self.mesh.ptdf['zone'].values == j 
                # sigma_w calculation (average of 2 phases)
                sigma_w0 = self.waxman['sigma_w0'][c]
                sigma_w1 = self.waxman['sigma_w1'][c]
                sat0 = self.waxman['sat0'][c]
                delta = sat[idx] - sat0 
                delta[delta<0] = 0 # cap delta 
                # sigma_w[delta == 0] = sigma_w0 
                sigma_w = ((delta/sat[idx])*sigma_w1)+((sat0/sat[idx])*sigma_w0)
                # compute conductivity 
                cond[idx] = waxmanSmit(sat[idx],
                                  self.waxman['F'][c],
                                  sigma_w,
                                  self.waxman['sigma_s'][c])
                c+=1 # add one to loop iterator index 
            
            # add conductivity values to data 
            self.nodResult['step%i'%i]['Conductivity'] = cond.tolist() 
            self.nodResult['step%i'%i]['Resistivity'] = (1/cond).tolist() 
            # covert to resistivity and map to elements 
            res[:,i] = self.mesh.node2ElemAttr(1/cond,'res%i'%i)
            
            # catch mutliple runs results as well 
            for run in run_keys:
                sat = np.array(self.nodResultMulti[run]['step%i'%i]['Saturation']) # pressure in kpa 
                sat[sat>1] = 1 # cap saturation at 1 
                c=0
                cond = np.zeros_like(sat)
                for j in np.unique(self.mesh.ptdf['zone'].values):
                    idx = self.mesh.ptdf['zone'].values == j 
                    sigma_w0 = self.waxman['sigma_w0'][c]
                    sigma_w1 = self.waxman['sigma_w1'][c]
                    sat0 = self.waxman['sat0'][c]
                    delta = sat[idx] - sat0 
                    delta[delta<0] = 0 # cap delta 
                    sigma_w = ((delta/sat[idx])*sigma_w1)+((sat0/sat[idx])*sigma_w0) # compute sigma_w 
                    # compute conductivity 
                    cond[idx] = waxmanSmit(sat[idx],
                                      self.waxman['F'][c],
                                      sigma_w,
                                      self.waxman['sigma_s'][c])
                    c+=1 # add one to loop iterator index 
                
                # add conductivity values to data 
                self.nodResultMulti[run]['step%i'%i]['Conductivity'] = cond.tolist() 
                self.nodResultMulti[run]['step%i'%i]['Resistivity'] = (1/cond).tolist() 
            
        return res 
    
    def res2sat(self,param='Resistivity'):
        """
        Convert resistivity back to saturation 

        Parameters
        ----------
        param : str, optional
            DESCRIPTION. The default is 'Resistivity'.

        Returns
        -------
        None.

        """
        if self.resultNsteps is None:
            raise Exception('Must run a model and get results first before converting to resistivity')

        if self.waxman['F'] is None:
            raise Exception('Formation factors not defined')
        if self.waxman['sigma_w'] is None:
            raise Exception('Pore fluid conductivity not defined')
        if self.waxman['sigma_s'] is None:
            raise Exception('Grain surface conductivity not defined')
            
        sw = np.zeros(self.mesh.numnp) # array to hold conductivity values 
        sat = np.zeros((self.mesh.numel,self.resultNsteps))
            
        for i in range(self.resultNsteps):
        # for i in tqdm(range(self.resultNsteps),ncols=100): # generally this bit of code is very quick 
            res = np.array(self.nodResult['step%i'%i][param]) # resisitivity?
            cond = 1/res 
            c=0
            for j in np.unique(self.mesh.ptdf['zone'].values):
                idx = np.argwhere(self.mesh.ptdf['zone'].values == j).flatten()
                for k in idx:
                    sw[k] = invWaxmanSmit(cond[k],
                                          self.waxman['F'][c],
                                          self.waxman['sigma_w'][c],
                                          self.waxman['sigma_s'][c])
                c+=1 # add one to loop iterator index 
            
            # add conductivity values to data 
            self.nodResult['step%i'%i]['Inverted_Saturation'] = sw.tolist() 
            # covert to resistivity and map to elements 
            sat[:,i] = self.mesh.node2ElemAttr(sw,'sat%i'%i)
            
        return sat
    
    #%% several run handling (monte carlo approach)
    def checkParam(self):
        if len(self.parg0.keys())==0:
            raise Exception('No input variables given to vary')

        allowed_param = ['k','theta','sat','res','alpha','vn']
        
        for key in self.parg0.keys():
            if key not in self.parg1.keys():
                raise Exception('Parameter %s not defined self.parg1')
            if key not in allowed_param:
                print('Parameters which can be varied = ')
                for a in allowed_param:
                    print(a)
                raise Exception('Parameter not allowed to be varied')
                
                
    def createPdir(self,runno, to_copy=[], pargs={}):
        if len(to_copy) == 0: 
            exts = ['INP', 'INP21A','BCS', 'ICS', 'RST', 
                    'BCOP', 'BCOPG', 'FIL', 'VG']
            files = os.listdir(self.dname)
           
            for f in files: # find files needed to run sutra 
                ext = f.split('.')[-1]
                if ext.upper() in exts: # add it to files which need copying 
                    to_copy.append(f)
                
        dpath = os.path.join(self.dname,self.template.format(runno))
        if not os.path.exists(dpath):
            os.mkdir(dpath) # create directory
        self.pdirs.append(dpath)
        # copy across files to parrallel run directory (files need to be written first)
        for j in range(len(to_copy)):
            source_path = os.path.join(self.dname,to_copy[j])
            copy_path = os.path.join(dpath,to_copy[j])
            shutil.copy(source_path,copy_path)
            
        # write run parameters 
        text = '{:<6s}\t'.format('')
        for i in range(self.nzones):
            text += '{:<8s}\t'.format('zone %i'%i)
        text += '\n'
        
        for key in pargs.keys():
            text += '{:<6s}\t'.format(key)
            for i in range(self.nzones):
                text += '{:<8e}\t'.format(pargs[key][i])
            text += '\n'
            
        fh = open(os.path.join(dpath,'pargs.txt'),'w')
        fh.write(text)
        fh.close() 
        
    
    def setupMultiRun(self,n=10):
        """
        Only setup for different porosity and permeability values for now. 
        Sets up directories for multiple runs with perturbed values. 

        Parameters
        ----------
        n : int, optional
            Number of runs. The default is 10.

        """
        nzone = self.nzones 
        self.checkParam() # get parg arguments are setup correctly 
        self.pdirs = []      
        self.nruns = 0 

        # set up matrices 
        theta = np.zeros((n,nzone))
        kperm = np.zeros((n,nzone))
        thetatrigger = False 
        ktrigger = False 
        
        pargs = {}
        
        zones = [i+1 for i in range(nzone)]
        
        self.runparam = {}
        
        if 'theta' in self.parg0.keys():
            for i in range(nzone):
                v0 = self.parg0['theta'][i] # end member 0 
                v1 = self.parg1['theta'][i] # end member 1 
                v = giveValues(v0,v1,n,self.pdist['theta'])
                theta[:,i] = v
            thetatrigger = True 
            
        if 'k' in self.parg0.keys():
            if 'grid' in self.pdist['k'] and nzone == 2:
                if n%10 != 0:
                    raise Exception('Cannot use grid based search unless n is a mutliple of 10')
                nlines = int(n/10)
                a = np.zeros((nlines,2))
                for i in range(nzone):
                    v0 = self.parg0['k'][i] # end member 0 
                    v1 = self.parg1['k'][i] # end member 1
                    if 'log' in self.pdist['k']:
                        a[:,i] = giveValues(v0,v1,nlines,'logordered')
                    else:
                        a[:,i] = giveValues(v0,v1,nlines,'ordered')
                m0,m1 = np.meshgrid(a[:,0],a[:,1])
                kperm = np.c_[m0.flatten('F'),m1.flatten('F')]
            else: # use montecarlo approach 
                for i in range(nzone):
                    v0 = self.parg0['k'][i] # end member 0 
                    v1 = self.parg1['k'][i] # end member 1 
                    v = giveValues(v0,v1,n,self.pdist['k'])
                    kperm[:,i] = v
            ktrigger = True 
        
        # set up running directories anc copy across relevant files 
        for i in range(n):
            if ktrigger: # then set new permeability values 
                self.setPerm(kperm[i,:])
                pargs['k'] = kperm[i,:]
                self.runparam['k'] = kperm 
            if thetatrigger: # set new porosity values 
                self.setPor(theta[i,:])
                pargs['theta'] = theta[i,:]
                self.runparam['theta'] = theta 
            self.writeInp()
            
            self.createPdir(i,pargs=pargs)
            self.nruns += 1  
            
    def runMultiRun(self):
        """
        Run SUTRA for multiple runs (uses joblib for parallisation)

        """
        if self.ncpu == 0 or self.ncpu == 1: # run single threaded with a basic for loop 
            pout = []
            for i in tqdm(range(self.nruns),ncols=100):    
                out = doSUTRArun(self.pdirs[i],self.execpath)
                pout.append(out)
        else: # run in parallel 
            pout = Parallel(n_jobs=self.ncpu)(delayed(doSUTRArun)(self.pdirs[i],self.execpath) for i in tqdm(range(self.nruns),ncols=100))
        
        for i,p in enumerate(pout):
            if p[1] == self.resultNsteps:
                self.nodResultMulti[i] = p[0]
        return pout 
    
    def clearMultiRun(self):
        """
        Clear directories made for multiple sutra runs. 

        """
        entries = os.listdir(self.dname)
        for e in entries:
            dpath = os.path.join(self.dname,e)
            if os.path.isdir(dpath):
                if 'pargs.txt' in os.listdir(dpath):
                    shutil.rmtree(dpath)
                    
    #%% coupled modelling with R2 family of codes 
    def setupRruns(self,surrogate_dir,flag_3d=False,run_keys=None):
        # find files to copy first 
        to_copy = []
        for e in os.listdir(surrogate_dir):
            fpath = os.path.join(surrogate_dir,e)
            if os.path.isfile(fpath):
                to_copy.append(fpath)
        ncols = 4
        if flag_3d:
            ncols = 5
        nrows = self.mesh.numel 
        
        if run_keys is None: 
            run_keys = self.nodResultMulti.keys()
            
        for run in run_keys:
            # note this will only do runs in stable solutions 
            dpath = os.path.join(self.dname,self.template.format(run))
            for f in to_copy: # copy across files needed for R2
                shutil.copy(f,dpath)
            
            # now to go and make all the resistivities at different steps 
            for i in range(self.resultNsteps):
                key = 'step%i'%i
                resNode = np.array(self.nodResultMulti[run][key]['Resistivity'])
                resElem = self.mesh.node2ElemAttr(resNode,'res%i'%run)
                matrix = np.zeros((nrows,ncols))
                matrix[:,-2] = resElem
                matrix[:,0] = self.mesh.df['X'].values
                if flag_3d:
                    matrix[:,1] = self.mesh.df['Y'].values
                    matrix[:,2] = self.mesh.df['Z'].values 
                else:
                    matrix[:,1] = self.mesh.df['Z'].values 
                matrix[:,-1] = np.log10(resElem)
                respath = os.path.join(dpath,'step{:0>4d}.dat'.format(i))
                np.savetxt(respath,matrix)
                
    def runResFwdmdls(self,execpath):
        idx = list(self.nodResultMulti.keys()) # index of stable solutions 
        nrun = len(idx)
        if self.ncpu == 0 or self.ncpu == 1: # run single threaded with a basic for loop 
            pout = [runR2runs(self.pdirs[idx[i]],execpath) for i in tqdm(range(nrun),ncols=100)] 
        else: # run in parallel 
            pout = Parallel(n_jobs=self.ncpu)(delayed(runR2runs)(self.pdirs[idx[i]],execpath) for i in tqdm(range(nrun),ncols=100))
        
        self.resFwdMdls = {}
        for i,p in enumerate(pout):
            if p[1] == self.resultNsteps:
                self.resFwdMdls[idx[i]] = p[0]
        return pout 
    
    #%% markov chain monte carlo running 
    def mcmc(self, tr, surrogate_dir, exe_path, nsteps=100, 
             noise=0.5, cfunc= None, scale_factor=1,
             target_ar = None, cache_runs = False, progress_bar=False):
        # steps: 
        # set mc trial parameters 
        # run trial parameters through SUTRA 
        # check if success 
        # convert to resistivity 
        # run resistivity through R2 
        # compute chi^2 value 
        # run metropolis algorithm to accept or decline new model 
        # do 1-4 for initial model 
        # repeat for as many steps needed 
        
        if cfunc is None:
            def cfunc(x):
                return x
        
        if target_ar is None:
            target_ar = -1 

        print('______Starting MCMC search______')
        # get starting place and set starting parameters 
        self.nodResultMulti = {}
        self.nruns = 0 
        naccept = 0 
        
        print('Starting search from initial model')
        model = {}
        model_trial = {}
        model_nparam = {}
        input_pargs = {}
        for key in self.pargs.keys():
            model[key] = self.pargs[key]
            model_nparam[key] = len(self.pargs[key])
            
        # setup dataframe to log mcmc chain 
        log = {}
        log['Chi^2'] = [0.0]*nsteps 
        log['Pt'] = [0.0]*nsteps 
        log['mu'] = [0.0]*nsteps 
        log['alpha'] = [0.0]*nsteps 
        log['Accept'] = [False]*nsteps 
        log['ar'] = [0.0]*nsteps 
        
        # setup parameters for altering the acceptance rate on the fly 
        ar = 1 # acceptance rate 
        a_fac = 1 # alpha factor 
        ac = [] # empty list for acceptance chain 
        
        # do first model 
        i=0 
        for key in self.pargs.keys():
            if key == 'k': # set model parameters if parameter is K 
                self.setPerm(model[key])
            if key == 'theta':
                self.setPor(model[key])
            for j in range(model_nparam[key]):
                log[key+str(j+1)] = [0.0]*nsteps # assign vector for storage 
                log[key+str(j+1)][i] = model[key][j]
                
        self.writeInp()
        self.createPdir(i,pargs=model) ## todo, put run number in here 
        self.nruns += 1 
        # run init parameters 
        data,n = doSUTRArun(self.pdirs[i],self.execpath)
        # check trial param are stable before proceeding 
        if n != self.resultNsteps: # n does not meet the expected number of steps 
            raise Exception('Initial trial is unstable solution! Try and different starting model') # dont accept the trial model 
    
        # add trial values to storage 
        self.nodResultMulti[i] = data 
        
        # convert result to resistivity for R2/(or R3t)
        self.sat2res2phase([i]) # do this for just the current run 
        self.setupRruns(surrogate_dir,False,[i]) # setup folder for running R2 
        
        # now run resistivity code 
        fout, n = runR2runs(self.pdirs[i],exe_path)
        
        # compute initial chi^2 value 
        d0 = np.array([]) # get real data values 
        d1 = np.array([]) 
        for key in tr.keys(): 
            d0 = np.append(d0, tr[key])
            d1 = np.append(d1,fout[key])
    
        
        residuals = d0 - d1 
        meas_errors = np.abs(d0*(noise/100))
        
        X2 = chi2_log(meas_errors, residuals)

        Pi = normLike(meas_errors, residuals)
        print('\nRun ',i)
        print('K1 = %f, K2 = %f'%(cfunc(model['k'][0]),cfunc(model['k'][1])))
        print('Pi = ',Pi)
        
        if progress_bar:
            flag = False 
            def dump(x):
                None 
        else:
            flag = True 
            def dump(x):
                print(x)

        # do random walk for model parameters
        # for loop goes here 
        for i in tqdm(range(1,nsteps),desc='MCMC',ncols=100,disable=flag):
            dump('\nRun %i '%i)
            
            accept = False 
            for key in self.pargs.keys():
                model_trial[key] = proposeStep(model[key],self.psize[key],self.pdist[key])
                input_pargs[key] = model_trial[key]
                if key == 'k': # set model parameters if parameter is K 
                    self.setPerm(model_trial[key])
                if key == 'theta':
                    self.setPor(model_trial[key])
                for j in range(model_nparam[key]): # cache model values 
                    log[key+str(j+1)][i] = model_trial[key][j]
                
            self.writeInp()
            self.createPdir(i,pargs=model_trial) 
            self.nruns += 1 
            dump('K1 = %f, K2 = %f'%(cfunc(model_trial['k'][0]),cfunc(model_trial['k'][1])))
            
            # check trial is within limits (otherwise move on)
            offlimit = False 
            for key in self.pargs.keys():
                for j in range(model_nparam[key]):
                    if model_trial[key][j] < self.parg0[key][j]: 
                        offlimit = True  
                        break 
                    if model_trial[key][j] > self.parg1[key][j]: 
                        offlimit = True  
                        break 
            if offlimit:
                dump('Proposed model is out of limits')
                dump('Accept = False')
                accept = False
                ac.append(0) # add one to acceptance chain so that unstable models dont get unfairly weighted 
                continue 
                
            # run trial parameters 
            data,n = doSUTRArun(self.pdirs[i],self.execpath)
            # check trial param are stable before proceeding 
            if n != self.resultNsteps: # n does not meet the expected number of steps 
                accept = False # dont accept the trial model 
                dump('Proposed model is unstable!')
                dump('Accept = False')
                ac.append(0)
                continue 
        
            # add trial values to storage 
            self.nodResultMulti[i] = data 
            
            # convert result to resistivity for R2/(or R3t)
            self.sat2res2phase([i]) # do this for just the current run 
            self.setupRruns(surrogate_dir,False,[i]) # setup folder for running R2 
            
            # now run resistivity code 
            fout, n = runR2runsP(self.pdirs[i],exe_path,ncpu=self.ncpu)
            
            # compute chi^2 value 
            d1 = np.array([]) 
            for key in tr.keys(): 
                d1 = np.append(d1,fout[key])
                
            if len(d0) != len(d1):
                accept = False # dont accept the trial model 
                dump('Proposed model is unstable!')
                dump('Accept = False')
                ac.append(0)
                continue 
            
            residuals = d0 - d1  # compare fout to 'real data' 
            meas_errors = np.abs(d0*(noise/100))
            
            X2 = chi2(meas_errors, residuals)
            
            Pt = normLike(meas_errors, residuals)
            
            dump('Pt = %f, Pi = %f'%(Pt,Pi))
            
            ## adaptation to metropolis ## 
            if target_ar < 0: # perform default metropolis 
                a_fac = 1 # hence alpha factor is 1 
            elif len(ac) < 10: # not enough samples in acceptance chain yet 
                a_fac = 1 # alpha factor is 1 
            elif Pt <= Pi:
                ar = sum(ac)/i # current acceptance rate 
                if ar == 0: 
                    # if acceptance rate is zero then default back to metropolis 
                    a_fac = 1 
                else: 
                    # adjust acceptance rate to match target acceptance rate 
                    a_fac = a_fac*(target_ar/ar)
                if a_fac >= 1: # cap alpha factor at 1 
                    a_fac = 1 
                else: # if alpha factor < 1 mention it in console 
                    dump('Acceptance probability adjusted*')
            
            ## metropolis algorithm ##
            alpha = (Pt/Pi)*a_fac
            mu = np.random.random() 
            if Pt > Pi:
                accept = True 
            elif alpha > mu:
                accept = True  
                
            dump('Alpha = %f, mu = %f'%(alpha,mu))
                
            log['Chi^2'][i] = X2 
            log['Pt'][i] = Pt 
            log['mu'][i] = mu 
            log['alpha'][i] = alpha 
            log['Accept'][i] = accept 
            log['ar'][i] = ar 
        
            # decide if to accept model 
            if accept:
                dump('Accept = True')
                for key in model.keys(): 
                    model[key] = model_trial[key]
                    #[model[key][j] + step[j] for j in range(model_nparam[key])]
                Pi = Pt 
                naccept += 1 
                ac.append(1)
            else:
                ac.append(0)
                dump('Accept = False')
                
            # delete new entry to save on memory 
            if not cache_runs:
                del self.nodResultMulti[i]
                
        return log, naccept/nsteps
    
    
    def getmcmcRuns(self,tr, surrogate_dir, exe_path, 
                    noise=0.5, cfunc= None,target_ar=0.2,cache_runs=False):
        """
        Use to get previous mcmc runs if still present in working directory 

        Returns
        -------
        None.

        """
        if cfunc is None:
            def cfunc(x):
                return x
        
        # find the run folders 
        entries = []
        for e in sorted(os.listdir(self.dname)):
            # check directory 
            if os.path.isdir(os.path.join(self.dname,e)):
                # check is p run 
                if e[0] == 'r' and e[-1].isdigit():
                    # files = os.path.join(self.dname,e)
                    entries.append(os.path.join(self.dname,e)) # add to list of entries
                    
        self.pdirs = entries 
        nsteps = len(self.pdirs) # get nsteps from previous runs 
                    
        # get starting place and set starting parameters 
        self.nodResultMulti = {}
        self.nruns = 0 
        naccept = 0 
        
        print('Starting (pretend) search from initial model')
        print('Steps found = %i'%nsteps)
        model = {}
        model_trial = {}
        model_nparam = {}
        for key in self.pargs.keys():
            model[key] = self.pargs[key]
            model_nparam[key] = len(self.pargs[key])
            
        # setup dataframe to log mcmc chain 
        log = {}
        log['Chi^2'] = [0.0]*nsteps 
        log['Pt'] = [0.0]*nsteps 
        log['mu'] = [0.0]*nsteps 
        log['alpha'] = [0.0]*nsteps 
        log['Accept'] = [False]*nsteps 
        log['ar'] = [0.0]*nsteps 
        
        # setup parameters for altering the acceptance rate on the fly 
        ar = 1 # acceptance rate 
        mem_length = 10 # length of acceptance chain 
        ac = [] # empty list for acceptance chain 
        
        # do first model 
        i=0 
        for key in self.pargs.keys():
            if key == 'k': # set model parameters if parameter is K 
                self.setPerm(model[key])
            if key == 'theta':
                self.setPor(model[key])
            for j in range(model_nparam[key]):
                log[key+str(j+1)] = [0.0]*nsteps # assign vector for storage 
                log[key+str(j+1)][i] = model[key][j]
                
            
        self.nruns += 1 
        # read in init parameters
        for f in os.listdir(self.pdirs[i]):
            if f.endswith('.nod'):
                break #this finds the .nod file 
        data,n = readNod(os.path.join(self.pdirs[i],f))
        
        # check trial param are stable before proceeding 
        if n != self.resultNsteps: # n does not meet the expected number of steps 
            raise Exception('Initial trial is unstable solution! Try and different starting model') # dont accept the trial model 
        
        # add trial values to storage 
        self.nodResultMulti[i] = data 
        
        
        # now run resistivity code 
        fout, n = runR2runsP(self.pdirs[i],exe_path)
        
        # compute initial chi^2 value 
        d0 = np.array([]) # get real data values 
        d1 = np.array([]) 
        for key in tr.keys(): 
            d0 = np.append(d0, tr[key])
            d1 = np.append(d1,fout[key])
        
        
        residuals = d0 - d1 
        meas_errors = np.abs(d0*(noise/100))
        
        X2 = chi2_log(meas_errors, residuals)
        
        Pi = normLike(meas_errors, residuals)
        print('\nRun ',i)
        print('K1 = %f, K2 = %f'%(cfunc(model['k'][0]),cfunc(model['k'][1])))
        print('Pi = ',Pi)
        
        # do random walk for model parameters (although not really becuase its already done)
        # for loop goes here 
        for i in range(1,nsteps):
        # def loop(i):
            print('\nRun ',i)
            
            if len(ac) == (mem_length+1): # remove 1st entry from acceptance chain
                _ = ac.pop(0)
                
            accept = False 
            # can read model trial parameters directly 
            model_trial = readParam(os.path.join(self.pdirs[i],'pargs.txt')) 
            for key in model_trial.keys(): 
                for j in range(model_nparam[key]): # cache model values 
                    log[key+str(j+1)][i] = model_trial[key][j]
                
            self.nruns += 1 
            print('K1 = %f, K2 = %f'%(cfunc(model_trial['k'][0]),cfunc(model_trial['k'][1])))
            
            # check trial is within limits (otherwise move on)
            offlimit = False 
            for key in self.pargs.keys():
                for j in range(model_nparam[key]):
                    if model_trial[key][j] < self.parg0[key][j]: 
                        offlimit = True  
                        break 
                    if model_trial[key][j] > self.parg1[key][j]: 
                        offlimit = True  
                        break 
                    
            if offlimit:
                print('Proposed model is out of limits')
                # print('Accept = False')
                accept = False
                ac.append(1) # add one to acceptance chain so that unstable models dont get unfairly weighted 
                continue 
                # return 
                
            # run trial parameters 
            data,n = readNod(os.path.join(self.pdirs[i],f))
            # check trial param are stable before proceeding 
            if n != self.resultNsteps: # n does not meet the expected number of steps 
                accept = False # dont accept the trial model 
                print('Proposed model is unstable!')
                # print('Accept = False')
                ac.append(1)
                continue 
                # return 
        
            # add trial values to storage 
            self.nodResultMulti[i] = data 
            
            # convert result to resistivity for R2/(or R3t) (not needed)
            self.sat2res2phase([i]) # do this for just the current run 
            self.setupRruns(surrogate_dir,False,[i]) # setup folder for running R2 
            
            # now run resistivity code 
            fout, n = runR2runsP(self.pdirs[i],exe_path)
            
            # compute chi^2 value 
            d1 = np.array([]) 
            for key in tr.keys(): 
                d1 = np.append(d1,fout[key])
            
            residuals = d0 - d1  # compare fout to 'real data' 
            meas_errors = np.abs(d0*(noise/100))
            
            X2 = chi2(meas_errors, residuals)
            
            Pt = normLike(meas_errors, residuals)
            
            print('Pt = %f, Pi = %f'%(Pt,Pi))
            
            ## adaptation to metropolis ## 
            if target_ar < 0: # perform default metropolis 
                a_fac = 1 # hence alpha factor is 1 
            elif len(ac) < mem_length: # not enough samples in acceptance chain yet 
                a_fac = 1 # a alpha factor is 1 
            elif Pt <= Pi:
                ar = sum(ac)/mem_length
                if ar == 0: 
                    # if acceptance rate is zero then default back to metropolis 
                    a_fac = 1 
                else: 
                    # adjust acceptance rate to match target acceptance rate 
                    a_fac = target_ar/ar
                    print('Acceptance probability adjusted*')
            
            ## metropolis algorithm ##
            alpha = (Pt/Pi)*a_fac
            mu = np.random.random() 
            if Pt > Pi:
                accept = True 
            elif alpha > mu:
                accept = True  
                
            print('Alpha = %f, mu = %f'%(alpha,mu))
                
            log['Chi^2'][i] = X2 
            log['Pt'][i] = Pt 
            log['mu'][i] = mu 
            log['alpha'][i] = alpha 
            log['Accept'][i] = accept 
            log['ar'][i] = ar 
        
            # decide if to accept model 
            if accept:
                # print('Accept = True')
                Pi = Pt 
                naccept += 1 
                ac.append(1)
            else:
                ac.append(0)
                # print('Accept = False')
                
            # delete new entry to save on memory 
            if not cache_runs:
                del self.nodResultMulti[i]
                
        return log
        
    
#%% convert input file to mesh file 
def mesh2dat(fname):
    # read in mesh file
    fh = open(fname, 'r')
    dump = fh.readlines()
    fh.close()
    nlines = len(dump)
    
    node_idx = 0
    connec_idx = 0
    node_dump = []
    connec_dump = []
    for i in range(nlines):
        line = dump[i].strip()
        if 'NODE' in line:
            node_idx = i
            # find where first entry on line is no longer a digit
            j = node_idx+1
            numnp = 0
            while True:
                l = dump[j].strip()
                if l[0] == '#':
                    pass  # then ignore
                elif l[0].isdigit():
                    numnp += 1
                    node_dump.append(l)
                else:
                    break
                j += 1
        if 'INCIDENCE' in line:
            connec_idx = i
            j = connec_idx+1
            numel = 0
            while True:
                l = dump[j].strip()
                if l[0] == '#':
                    pass  # then ignore
                elif l[0].isdigit():
                    numel += 1
                    connec_dump.append(l)
                else:
                    break
                j += 1
                if j == nlines:  # break if at bottom of file
                    break
    
    # create arrays
    connec = np.zeros((numel, 4), dtype=int)
    node = np.zeros((numnp, 3), dtype=float)
    
    for i in range(numnp):
        info = node_dump[i].split()
        node[i, 0] = float(info[2])
        node[i, 2] = float(info[3])
    
    for i in range(numel):
        info = connec_dump[i].split()
        for j in range(4):
            connec[i, j] = int(info[j+1])-1
            
    #write to file 
    fh = open(fname.replace('.inp','.dat'))
    numel = connec.shape[0]
    numnp = node.shape[0]
    fh.write('%i %i 0\n'%(numel,numnp))
    for i in range(numel):
        fh.write('%i\t'%(i+1))
        for j in range(connec.shape[1]):
            fh.write('%i\t'%connec[i,j])
        fh.write('\n')
    for i in range(numnp):
        fh.write('%i\t'%(i+1))
        for j in range(node.shape[1]):
            fh.write('%f\t'%node[i,j])
        fh.write('\n')
    fh.close()
